import React from "react";
import { Link } from "react-router-dom";
import "@/styles/style.less";
import logo from "@/resource/images/logo.png";

const Header = () => {
  return (
    <div className="header">
      <div className="wrap clearfix">
        <Link className="logo fl" to="/">
          <img alt="logo" className="imgset" src={logo} />
        </Link>
        <div className="menu clearfix fr">
          <Link to="/">网站首页</Link>
          <Link to="/">新闻资讯</Link>
          <Link to="/">操作教程</Link>
          <Link to="/">用户疑问</Link>
          <Link to="/">联系我们</Link>
        </div>
      </div>
    </div>
  );
};

export default Header;
