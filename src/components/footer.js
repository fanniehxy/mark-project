import React from "react";
import { Link } from "react-router-dom";
import "@/styles/style.less";

import ewm from "@/resource/images/ewm.png";

const Footer = () => {
  return (
    <div className="footer">
      <div className="foot_top">
        <div className="wrap">
          <div className="footmenu clearfix">
            <Link to="/">关于我们</Link>

            <Link to="/">联系我们</Link>
            <Link to="/">声明免责</Link>
            <Link to="/">服务条款</Link>
            <Link to="/">常见新闻</Link>
            <Link to="/">行业新闻</Link>
            <Link to="/">热点新闻</Link>

            <Link to="/">号码标记查询</Link>
            <Link to="/">号码标记取消</Link>
          </div>
        </div>
      </div>
      <div className="foot_bot">
        <div className="wrap clearfix">
          <div className="foot_bot_l fl">
            <div className="fot_tabc">
              <div className="fot_tabc_hd">
                <span className="active">
                  最新资讯<i></i>
                </span>
                <span>
                  公司介绍<i></i>
                </span>
              </div>
              <div className="fot_tab_bdcont">
                <div className="fot_tabitem">
                  <div className="clearfix yqcont">
                    <Link to="/" title="号码标记真能取消吗？[2020-07-08]">
                      号码标记真能取消吗？
                    </Link>

                    <Link
                      to="/"
                      title="中国的卫星电话来了，一年1000元可通话750分钟,号码标记情况怎么样吗？[2020-07-08]"
                    >
                      中国的卫星电话来了，一年1000元可通话750分钟,号码标记情况怎么样吗？
                    </Link>

                    <Link
                      to="/"
                      title="号码标记查询关注:12306正研究一次买齐往返票[2020-07-08]"
                    >
                      号码标记查询关注:12306正研究一次买齐往返票
                    </Link>

                    <Link
                      to="/"
                      title="号码标记查询关注:12306正研究一次买齐往返票[2020-07-08]"
                    >
                      号码标记查询关注:12306正研究一次买齐往返票
                    </Link>

                    <Link
                      to="/"
                      title="如何关注“万号标记微信公众号”请求帮助[2020-07-08]"
                    >
                      如何关注“万号标记微信公众号”请求帮助
                    </Link>

                    <Link
                      to="/"
                      title="如何关注“万号标记微信公众号”请求帮助[2020-07-08]"
                    >
                      如何关注“万号标记微信公众号”请求帮助
                    </Link>
                  </div>
                </div>
                <div className="fot_tabitem">
                  广州万户网络科技有限公司是专业从事互联网相关业务及号码标记查询取消网,提供：号码标记查询、自动化开发,为企业提供解决方案的一体的综合性公司
                </div>
              </div>
            </div>
            <div className="fot_txt">
              <p>
                CopyRight © 2003 - 2019 版权所有：广州万户网络科技有限公司
                工信部备案{" "}
                <Link rel="nofollow" to="/" target="_blank">
                  粤ICP备19078691号-2
                </Link>
                <Link to="/" target="_blank" title="站长统计">
                  站长统计
                </Link>
              </p>
            </div>
          </div>
          <div className="fr clearfix foot_bot_r mt_30">
            <div className="ewmbox fr">
              <img alt="微信公众号" className="imgset" src={ewm} />
              <p>微信公众号</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
