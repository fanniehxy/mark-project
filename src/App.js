import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Header from "@/components/header";
import Footer from "@/components/footer";
import Index from "@/app/index";
import Search from "@/app/search";

import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" component={Index}></Route>
        <Route exact path="/search/:tele" component={Search}></Route>
      </Switch>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
