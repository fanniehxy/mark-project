import React from "react";
import "../../../styles/style.less";

import steps from "@/resource/images/steps.jpg";
import index2_31 from "@/resource/images/index2_31.jpg";
import index2_34 from "@/resource/images/index2_34.jpg";

const Section = () => {
  return (
    <>
      <div className="section">
        <div className="wrap">
          <div className="sec_tt text-center">
            <h3>操作流程</h3>
            <div className="posrelative text-center sec_subtt">
              <em className="lem">
                <i></i>
              </em>
              <span>operation</span>
              <em className="rem">
                <i></i>
              </em>
            </div>
          </div>
          <div className="mt_30">
            <img alt="" className="imgset" src={steps} />
          </div>
        </div>
      </div>
      <div className="section bggray">
        <div className="wrap">
          <div className="sec_tt text-center">
            <h3>用户评价</h3>
            <div className="posrelative text-center sec_subtt">
              <em className="lem">
                <i></i>
              </em>
              <span>evaluation</span>
              <em className="rem">
                <i></i>
              </em>
            </div>
          </div>
          <div className="pjlist">
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_31} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>138****6808</span>于
                  <em>2020-07-28&nbsp;11:49:24</em>发表评价
                </div>
                <div className="pdr_b">
                  真心感谢客服耐心的指导，我手机号码标记已经取消了，没有显示了，如果不是有这个标记查询工具，我还真打算放弃了！有些标记平台根本找不到取消入口，还有部分标记平台取消需要一大堆的资料等，没有专业的指导，还真没有办法操作！太感谢了。
                </div>
              </div>
            </div>
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_34} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>132****3156</span>于
                  <em>2020-07-28&nbsp;12:21:18</em>发表评价
                </div>
                <div className="pdr_b">
                  被错误的标记了，很烦！客户都不接我电话了，后来找到这个万号标记查询系统，开始以为他们是标记平台方，而且还收费，感觉很不合理，后来经过客服耐心讲解，原来他们是一个标记查询工具，收的是查询费，而且免费提供专业的取消教程，教程简单好操作，查询服务有效期为1年，太有用了，收藏了。
                </div>
              </div>
            </div>
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_34} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>189****9998</span>于
                  <em>2020-07-28&nbsp;17:28:09</em>发表评价
                </div>
                <div className="pdr_b">
                  感谢！整合了很多标记平台在里面，如果一个个的去找取消的话，也不知道等到猴年马月了，用了这个标记查询工具后，给我省下不少时间，好用，操作简单方便，我介绍给我同事了，他们也觉得好用，开心！真要谢谢老板！
                </div>
              </div>
            </div>
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_34} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>139****9876</span>于
                  <em>2020-07-28&nbsp;08:21:55</em>发表评价
                </div>
                <div className="pdr_b">
                  自己操作后，以为马上生效的，弄了半天，找客服问了才知道，是部分标记平台立即生效，有部分标记平台是要等7天审核的，收到审核通过短信后，经常有联系的手机可能会有缓存问题，需要等的时间会稍微再长一些，不过我的3天后就没有了，感谢万号标记查询系统。
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="section bggray">
        <div className="wrap">
          <div className="sec_tt text-center">
            <h3>用户评价</h3>
            <div className="posrelative text-center sec_subtt">
              <em className="lem">
                <i></i>
              </em>
              <span>evaluation</span>
              <em className="rem">
                <i></i>
              </em>
            </div>
          </div>
          <div className="pjlist">
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_34} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>138****6808</span>于
                  <em>2020-07-28&nbsp;11:49:24</em>发表评价
                </div>
                <div className="pdr_b">
                  真心感谢客服耐心的指导，我手机号码标记已经取消了，没有显示了，如果不是有这个标记查询工具，我还真打算放弃了！有些标记平台根本找不到取消入口，还有部分标记平台取消需要一大堆的资料等，没有专业的指导，还真没有办法操作！太感谢了。
                </div>
              </div>
            </div>
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_34} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>132****3156</span>于
                  <em>2020-07-28&nbsp;12:21:18</em>发表评价
                </div>
                <div className="pdr_b">
                  被错误的标记了，很烦！客户都不接我电话了，后来找到这个万号标记查询系统，开始以为他们是标记平台方，而且还收费，感觉很不合理，后来经过客服耐心讲解，原来他们是一个标记查询工具，收的是查询费，而且免费提供专业的取消教程，教程简单好操作，查询服务有效期为1年，太有用了，收藏了。
                </div>
              </div>
            </div>
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_34} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>189****9998</span>于
                  <em>2020-07-28&nbsp;17:28:09</em>发表评价
                </div>
                <div className="pdr_b">
                  感谢！整合了很多标记平台在里面，如果一个个的去找取消的话，也不知道等到猴年马月了，用了这个标记查询工具后，给我省下不少时间，好用，操作简单方便，我介绍给我同事了，他们也觉得好用，开心！真要谢谢老板！
                </div>
              </div>
            </div>
            <div className="pjitem clearfix">
              <div className="fl pjimg">
                <img alt="" src={index2_34} />
              </div>
              <div className="pj_r fl">
                <div className="pjr_t">
                  用户：<span>139****9876</span>于
                  <em>2020-07-28&nbsp;08:21:55</em>发表评价
                </div>
                <div className="pdr_b">
                  自己操作后，以为马上生效的，弄了半天，找客服问了才知道，是部分标记平台立即生效，有部分标记平台是要等7天审核的，收到审核通过短信后，经常有联系的手机可能会有缓存问题，需要等的时间会稍微再长一些，不过我的3天后就没有了，感谢万号标记查询系统。
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Section;
