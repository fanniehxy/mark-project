import React, { useState } from "react";
import { Link } from "react-router-dom";
import "@/styles/style.less";
import slogan from "@/resource/images/slogan.png";
import index2_15 from "@/resource/images/index2_15.png";

const Banner = () => {
  const [tele, setTele] = useState();

  return (
    <div className="banner">
      <div className="banner_cont">
        <p>
          <img alt="" className="imgsetw" src={slogan} />
        </p>
        <div className="search_c mt_30">
          <p className="clearfix">
            <input
              onKeyUp={(e) => setTele(e.target.value)}
              placeholder="在此输入手机号、固话、400电话查询"
              className="sipt fl"
              type="text"
              name="mobile"
              id="mobile"
            />
            <Link
              to={`/search/${tele}`}
              className="submitbtn fl"
              id="sub_search"
            >
              标记查询
            </Link>
            <input name="f" type="hidden" id="f" value="mobile" />
          </p>
        </div>
        <div className="clearfix mt_30">
          <img alt="联通" className="imgsetw" src={index2_15} />
        </div>
      </div>
    </div>
  );
};

export default Banner;
