import React from "react";
import Banner from "@/app/index/components/banner";
import Section from "@/app/index/components/section";
const Index = () => {
  return (
    <>
      <Banner />
      <Section />
    </>
  );
};

export default Index;
