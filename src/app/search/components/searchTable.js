import React, { useEffect } from "react";
import { Link, useParams } from "react-router-dom";

import "@/styles/style.less";

const SearchTable = () => {
  const { tele } = useParams();
  useEffect(() => {
    setTimeout(() => {
      console.log(24234);
    }, 2000);
  }, []);

  return (
    <>
      <div className="brdcont">
        <div className="wrap">
          <div className="bread clearfix">
            <div className="fl bread_l">
              查询到您的号码：<span>{tele}</span>被<em id="ptnum">0</em>
              个平台标记
            </div>
            <div className="bread_r fr">
              <Link to="/">
                <i className="fa fa-arrow-circle-left"></i>&nbsp;返回重新查询
              </Link>
              <input
                type="hidden"
                name="mobile"
                id="mobile"
                value="15273184562"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="wrap">
          <div className="main">
            <div className="datacont">
              <table
                className="rs_table fullwidth"
                id="rs_table"
                border="0"
                cellSpacing="0"
                cellPadding="0"
              >
                <tbody>
                  <tr>
                    <th width="32%">标记号码的平台</th>
                    <th width="32%">标记名称</th>
                    <th width="18%">自助取消入口</th>
                    <th>取消教程</th>
                  </tr>
                </tbody>
              </table>
              <div id="content-msg" className="text-center">
                <br />
                <h1>简单查询了一下，未发现被标记数据！</h1>
                <br />
                <h1>您可免费点击下方【点击显示更多标记】按钮，进行全面查询</h1>
                <br />
              </div>
              <div className="text-center">
                <div>
                  <button
                    className="btnmycaptcha"
                    id="TencentCaptcha"
                    data-appid="2067757449"
                    data-cbfn="myCaptcha"
                    data-biz-state="15273184562"
                  >
                    点击显示更多标记
                  </button>
                </div>{" "}
                <br />
              </div>
              <span id="getpayment" name="getpayment"></span>
              <div className="tips text-center" style={{ display: "none" }}>
                按手机号码来收费，一个手机号码只需支付{" "}
                <font color="#FF0000" style={{ fontSize: "18px" }}>
                  32 元
                </font>{" "}
                查询技术费，可以显示上面 所有 内容， 快捷的使用自助取消标记服务!
              </div>
              <div className="text-center mt_20">
                <div className="paycont mt_20">
                  <button className="paybtn" to="/" id="searchBuy">
                    点击支付费用
                  </button>
                </div>
                <br />
                <div className="mt_20">
                  <Link
                    className="hovercolor cl9 txtline"
                    style={{ fontSize: "22px" }}
                    to="/"
                    target="_blank"
                  >
                    <font color="#FF0000">[联系微信客服,想了解一下]</font>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SearchTable;
